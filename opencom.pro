#-------------------------------------------------
#
# Project created by QtCreator 2013-03-13T12:45:18
#
#-------------------------------------------------

include(server/server.pri)
include(client/client.pri)

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = opencom
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

OTHER_FILES += \
    server/server.pri \
    client/client.pri \
    docs/README.md
